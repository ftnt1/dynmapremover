#!/usr/bin/env python3

"""dynmapremover.py: It removes dynamic mappings from "exe fmpolicy" object dump"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.0"
__status__ = "Production"

import os
import sys


BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal and displaying welcome message
os.system('cls' if os.name == 'nt' else 'clear')
print(GREEN + '\n\n\n\n\nWelcome!\nThis is FortiManager Dynamic Mapping remover ' + RED + 'v1.0\n' + NORM)


# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .txt files to list "cfgfiles"
for file in dirs:
    if ".txt" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
elif cfgindex == 1:
    print('\nOnly one file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
else:
    print('\nNo *.txt, *.conf or *.log files found. Exit!')
    sys.exit()
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()


flag = 1
newtable = []

for line in cfg:
    if line.startswith("config dynamic_mapping"):
        newtable.append(line)
        flag = 0
    if line.startswith("end"):
        flag = 1
    if not flag and line.startswith("edit"):
        line = str.replace(line, "edit", "delete")
        newtable.append(line)
    if flag and not line.startswith("set"):
       newtable.append(line)

filename = "./new_" + cfgfile
newfile = open(filename,'w+')

obj = 0
flag = 1
write = 0
objtable = []
for line in newtable:
    if line.startswith("config firewall address"):
        objtable.append([])
        objtable[obj].append(line)
        write = 1
        obj += 1
        newfile.write(line)
    if line.startswith("edit"):
        objtable.append([])
        objtable[obj].append(line)
    if line.startswith("end"):
        objtable[obj].append(line)
    if line.startswith("next"):
        objtable[obj].append(line)
        obj += 1
    if line.startswith("delete"):
        objtable[obj].append(line)
    if line.startswith("config dynamic_mapping"):
        objtable[obj].append(line)

for o in objtable:
    if any("delete" in s for s in o):
        for line in o:
           newfile.write(line)

newfile.close()
