# README #

FortiManager Dynamic Mapping Remover

## Files ##

### dynmapremover.py ###

This script will take file containing "exe fmpolicy print-adom-object" firewall address object dump as an input and delete all dynamic mappings

It takes no command line arguments

### README.md ###

This file

## Usage ##

Run it from within the directory containing .txt file with dump (like addresses.txt)

### Example: ###

#### Input "addr.txt" ####
```
VM-GVA-FMG01 # execute fmpolicy print-adom-object 226 all

Dump all objects for category [firewall address] in adom [GunvorGlobal]:
---------------
config firewall address
edit "ANR-RM1-FWL-INT-01A"
set uuid 889cb176-7433-51e6-8045-2b4d02ed5769
set comment "ANR-RM1-FWL-INT-01A"
set subnet 172.16.6.122 255.255.255.255

next

edit "BAH_INT_CLIENT_10.40.101.0/24"
set uuid 37ac8d6c-2290-51e6-9f8c-f03270e08852
set comment "BAH_INT_CLIENT_10.40.101.0/24"
set subnet 10.40.101.0 255.255.255.0
config dynamic_mapping
edit "MOS-DC-FWL-CLU"-"root"
set associated-interface "any"
set subnet 10.40.101.0 255.255.255.0
set comment "BAH_INT_CLIENT_10.40.101.0/24"
set uuid b0ab0506-97d6-51e7-7646-aec40db8f202
next
end

next

edit "BAH_INT_CLIENT_B_10.40.104.0/24"
set uuid 37ac9c3a-2290-51e6-a34c-6e77a912ffaf
set comment "BAH_INT_CLIENT_B_10.40.104.0/24"
set subnet 10.40.104.0 255.255.255.0
config dynamic_mapping
edit "MOS-DC-FWL-CLU"-"root"
set associated-interface "any"
set subnet 10.40.104.0 255.255.255.0
set comment "BAH_INT_CLIENT_B_10.40.104.0/24"
set uuid b0ab374c-97d6-51e7-cf8c-65f9b936a245
next
end

end

VM-GVA-FMG01 # 
```
#### Output "new_addr.txt": ####
```
VM-GVA-FMG01 # execute fmpolicy print-adom-object 226 all

Dump all objects for category [firewall address] in adom [GunvorGlobal]:
---------------
config firewall address
edit "ANR-RM1-FWL-INT-01A"

next

edit "BAH_INT_CLIENT_10.40.101.0/24"
config dynamic_mapping
delete "MOS-DC-FWL-CLU"-"root"
end

next

edit "BAH_INT_CLIENT_B_10.40.104.0/24"
config dynamic_mapping
delete "MOS-DC-FWL-CLU"-"root"
end

end

VM-GVA-FMG01 # 
```
